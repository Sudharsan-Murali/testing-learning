# Testing - Introduction:

Software testing, also called Quality Assurance (QA), is one of the most important steps of the Software Development Life Cycle (SDLC).

Quality is extremely hard to explain. It can be simply stated as `Fit for Use`. The quality of a product involves meeting customer requirements, including but not limited to functionality, design, reliability,and durability while keeping the software product usable without any issues.

Assurance is noing but providing a garuntee that the product will run well without any issues. This can only be done when the product has been extensively tested for bugs or issues.

In order for effective testing to take place, the tester must be knowledgeable in as many areas as possible. This includes the type of product, the technology used, pros and cons of the tech and environment used, the various delivery methodologies, testing methodologies, etc.,

In addition toall the above and more, the tester also needs a naturally curious mind, and a testing mindset. He / She needs to be curious to the point where they are obsessed with teir object of curiosity. They should be open to tinker and take risks. Cause, the best way to learn is by doing stuff.
